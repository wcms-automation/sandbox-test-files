#!/bin/bash

path=https://wms-profiles.uwaterloo.ca
test=/usr/local/bin/profile-test-files
echo ''
echo 'Checking xml_output folder for previous tests...'
cd $test/xml_output

rm -f *.xml

echo 'Cleared folder...'
echo ''
echo 'Cleaning Simple Test Environment before testing...'

cd /var/www/$1/
php scripts/run-tests.sh --url $path/$2 --clean
echo ''

echo 'Cleaned, ready to test...'

echo ''
echo 'Starting to run SimpleTest...'
echo ''

cd /var/www/$1/
php scripts/run-tests.sh --url $path/$2 --xml $test/xml_output "UW WCMS"

echo 'SimpleTests have completed'

