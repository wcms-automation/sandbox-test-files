
#!/bin/bash

cd /var/www/$1/sites/ca.$2

drush cc all
drush updb -y
drush fra -y
drush cc all
drush en simpletest -y
drush en site_test -y

echo 'Cleared caches, updated the database, reverted features and enabled the testing modules.'
